#include "SDL.h"
#include "SDL_tbmf.h"
#include "SDL_tbmf_drawtext.h"
#include "SDL_tbmf_errors.h"
#include "SDL_tbmf_textsurf.h"
#include <cstring>
#include <sys/types.h>

namespace sdl_tbmf {

using namespace sdl_tbmf::errors;

TextSurface::TextSurface(const char *t, const Font &f, SDL_Surface *s):
 mText(t),
 mFont(f),
 mTexture(s) {
	SDL_Color white = { 0xff, 0xff, 0xff, 0 };
	mColor = white;
	mSurface = NULL;
	generate();
}

TextSurface::TextSurface(const char *t, const Font &f, SDL_Color c):
 mText(t),
 mFont(f),
 mColor(c),
 mTexture(NULL) {
	mSurface = NULL;
	generate();
}

TextSurface::~TextSurface() {
	if (mSurface) SDL_FreeSurface(mSurface);
}

TextSurface &TextSurface::setText(const char *t) {
	mText = t;
	generate();
	return *this;
}

TextSurface &TextSurface::setFont(const Font &f) {
	mFont = f;
	generate();
	return *this;
}

TextSurface &TextSurface::setTexture(SDL_Surface *t) {
	mTexture = t;
	generate();
	return *this;
}

TextSurface &TextSurface::setColor(SDL_Color c) {
	mColor = c;
	mTexture = NULL;
	generate();
	return *this;
}

void TextSurface::generate() {
	if (mSurface) SDL_FreeSurface(mSurface);

	Uint32 width = 0;
	for (unsigned int i = 0; i < mText.length(); ++i)
		width += mFont.getCharacterWidth(u_char(mText[i])) + 1;
	--width;

	mSurface = SDL_CreateRGBSurface(SDL_SRCALPHA, width, mFont.height(),
					32, 0x00ff0000, 0x0000ff00,
					0x000000ff, 0xff000000);
	if (mSurface == NULL)
		throw Error(etSurfaceCreate, SDL_GetError());
	SDL_Surface *source = SDL_CreateRGBSurface(SDL_SRCALPHA, width,
						   mFont.height(), 32,
						   0x00ff0000, 0x0000ff00,
						   0x000000ff, 0xff000000);
	if (source == NULL)
		throw Error(etSurfaceCreate, SDL_GetError());

	SDL_FillRect(source, NULL, 0xffffffff);
	drawText(source, 0, 0, mText, mFont);

	if (mTexture == NULL)
		SDL_FillRect(mSurface, NULL, SDL_MapRGB(mSurface->format,
							mColor.r, mColor.g,
							mColor.b));
	else {
		SDL_Rect rect;
		for (rect.y = 0; rect.y < mSurface->h; rect.y += mTexture->h)
			for (rect.x = 0; rect.x < mSurface->w;
			     rect.x += mTexture->w)
				SDL_BlitSurface(mTexture, NULL,
						mSurface, &rect);
	}

	if (SDL_LockSurface(source) != 0)
		throw Error(etSurfaceLock, SDL_GetError());
	if (SDL_LockSurface(mSurface) != 0)
		throw Error(etSurfaceLock, SDL_GetError());
	Uint32 *pf = (Uint32 *)source->pixels;
	Uint32 *pt = (Uint32 *)mSurface->pixels;
	Uint32 maxI = source->w * source->h;
	Uint8 rf, rt, g, b, a;
	for (unsigned int i = 0; i < maxI; ++i) {
		SDL_GetRGBA(*pf, source->format, &rf, &g, &b, &a);
		SDL_GetRGBA(*pt, mSurface->format, &rt, &g, &b, &a);
		*pt = SDL_MapRGBA(mSurface->format, rt, g, b, ~rf);
		++pt;
		++pf;
	}
	SDL_UnlockSurface(source);
	SDL_UnlockSurface(mSurface);

	SDL_FreeSurface(source);
}

} // namespace sdl_tbmf
