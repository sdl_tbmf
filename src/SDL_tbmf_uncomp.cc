#include "SDL_tbmf_errors.h"
#include "SDL_tbmf_uncomp.h"
#include <SDL.h>
#include <cstdlib>
#include <iostream>
#include <vector>

namespace sdl_tbmf {

using namespace sdl_tbmf::errors;
using std::vector;

void putByteGroup(u_char, Uint32, vector<u_char> &);

bool uncompressData(Data *input, u_char special) {
	if (input->mSize == 0) return false;

	vector<u_char> v;

	u_char b;
	for (Uint32 i = 0; i < input->mSize; ++i) {
		b = input->mData[i];
		if (b == special) {
			if (i + 2 < input->mSize) {
				if (input->mData[i + 1] == special)
					putByteGroup(special,
						     input->mData[i + 2] + 1,
						     v);
				else putByteGroup(input->mData[i + 1],
						  input->mData[i + 2] + 4, v);
				i += 2;
			}
			else {
				v.clear();
				return false;
			}
		}
		else putByteGroup(b, 1, v);
	}

	delete input->mData;

	input->mData = new u_char[v.size()];
	if (input->mData == 0)
		throw Error(etNoMem);

	input->mSize = v.size();

	// Should be done more efficient way, but...
	for (Uint32 i = 0; i < v.size(); ++i)
		input->mData[i] = v[i];

	return true;
}

void putByteGroup(u_char b, Uint32 n, vector<u_char> &v) {
	while (n--) v.push_back(b);
}

} // namespace sdl_tbmf
