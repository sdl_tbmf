#ifndef SDL_TBMF_ERRORS_H
#define SDL_TBMF_ERRORS_H

namespace sdl_tbmf {
namespace errors {

enum ErrorType {
	etSuccess, etNoError = 0, etNone = 0, // No errors
	etFailure, etUnknown = 1, // Unknown/unspecified error
	etNoMem,
	etFileOpen,
	etFileFormat,
	etFileRead,
	etSurfaceCreate,
	etSurfaceLock,
	etOutsideArray,
	etCharMapZeroIndex,
	etCharMapNoNode,
	etPaletteSet,
	etNumberOfErrors
};

class Error {
public:
	Error(ErrorType e = etUnknown, const char *s = ""):
		mType(e), mString(s) {}
	Error(const char *s): mType(etUnknown), mString(s) {}

	bool operator==(const Error &o) const { return (mType == o.mType); }

	void print() const;

	ErrorType type() const { return mType; }
	const char *toString() const { return mString; }

private:
	ErrorType mType;
	const char *mString;

	static const char *const mErrorStrings[etNumberOfErrors];
};

} // namespace errors
} // namespace sdl_tbmf

#endif /* SDL_TBMF_ERRORS_H */
