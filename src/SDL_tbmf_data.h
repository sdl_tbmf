#ifndef SDL_TBMF_DATA_H
#define SDL_TBMF_DATA_H

namespace sdl_tbmf {

struct Data {
	u_char *mData;
	unsigned int mSize;
};

} // namespace sdl_tbmf

#endif /* SDL_TBMF_DATA_H */
