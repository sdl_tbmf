#ifndef SDL_TBMF_TEXTSURF_H
#define SDL_TBMF_TEXTSURF_H

#include "SDL.h"
#include "SDL_tbmf.h"
#include "SDL_tbmf_string.h"

namespace sdl_tbmf {

class TextSurface {
public:
	TextSurface(const char *, const Font &, SDL_Surface * = NULL);
	TextSurface(const char *, const Font &, SDL_Color);
	~TextSurface();

	void operator=(const char *t) { setText(t); }
	operator SDL_Surface *() { return mSurface; }
	operator const SDL_Surface *() const { return mSurface; }

	SDL_Surface *surface() { return mSurface; }
	const SDL_Surface *surface() const { return mSurface; }

	char *text() { return mText; }
	const char *text() const { return mText; }
	Font &font() { return mFont; }
	const Font &font() const { return mFont; }
	SDL_Color color() const { return mColor; }
	SDL_Surface *texture() { return mTexture; }
	
	TextSurface &setText(const char *);
	TextSurface &setFont(const Font &);
	TextSurface &setColor(SDL_Color);
	TextSurface &setTexture(SDL_Surface *);

private:
	TString mText;
	Font mFont;
	SDL_Color mColor;
	SDL_Surface *mTexture;
	SDL_Surface *mSurface;

	void generate();
};

} // namespace sdl_tbmf

#endif /* SDL_TBMF_TEXTSURF_H */
