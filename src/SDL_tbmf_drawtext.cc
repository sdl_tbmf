#include "SDL_tbmf.h"
#include "SDL_tbmf_drawtext.h"
#include <cstring>
#include <sys/types.h>

namespace sdl_tbmf {

void drawChar(SDL_Surface *, Sint16, Sint16, wchar_t, Font &);

void drawText(SDL_Surface *sf, Sint16 x, Sint16 y,
	      const char *text, Font &font) {
	if (text == NULL) return;
	for (unsigned int i = 0; i < strlen(text); ++i) {
		drawChar(sf, x, y, u_char(text[i]), font);
		x += font.getCharacterWidth(u_char(text[i])) + 1;
	}
}

int drawTextBox(SDL_Surface *sf, Sint16 x, Sint16 y, Sint16 w, Sint16 h,
		const char *text, Font &font) {
	if (text == NULL) return 1;
	if (font.height() > h) return 0;

	Sint16 a = 0;

	for (unsigned int i = 0; i < strlen(text); ++i) {
		if (a + font.getCharacterWidth(u_char(text[i])) <= w)
			drawChar(sf, x + a, y, u_char(text[i]), font);
		else return 0;
		a += font.getCharacterWidth(u_char(text[i])) + 1;
	}
	return 1;
}

void drawChar(SDL_Surface *sf, Sint16 x, Sint16 y, wchar_t c, Font &font) {
	SDL_Rect dstRect = {x, y, 0, 0};
	SDL_BlitSurface(const_cast<SDL_Surface *>(font.getCharacter(c)),
			NULL, sf, &dstRect);
}

/*
void drawChar(Surfake sf, Sint16 x, Sint16 y, wchar_t c,
	      Font &font, SDL_Color color) {
	SDL_Rect dstRect;
	Surfake tmp, cSurf;
	Uint32 *buf1, *buf2, i;
	Uint8 r, g, b, a, tr, tg, tb;

	dstRect.x = x;
	dstRect.y = y;
	r = color.r;
	g = color.g;
	b = color.b;

	cSurf = font.Character(c);

	tmp = SDL_CreateRGBSurface(SDL_SRCALPHA, cSurf->w, cSurf->h,
	 32, 0, 0, 0, 0xff000000);

	if (!tmp) return;

	SDL_LockSurface(tmp);
	SDL_LockSurface(cSurf);

	buf1 = (Uint32 *)cSurf->pixels;
	buf2 = (Uint32 *)tmp->pixels;

	for (i = 0; i < Uint32(tmp->w * tmp->h); i++, buf1++, buf2++) {
		SDL_GetRGBA(*buf1, cSurf->format, &tr, &tg, &tb, &a);
		*buf2 = SDL_MapRGBA(tmp->format, r, g, b, a);
	}

	SDL_UnlockSurface(cSurf);
	SDL_UnlockSurface(tmp);

	SDL_BlitSurface(tmp, NULL, sf, &dstRect);

	SDL_FreeSurface(tmp);
}
*/

} // namespace sdl_tbmf
