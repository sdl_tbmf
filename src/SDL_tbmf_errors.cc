#include "SDL_tbmf_errors.h"
#include <iostream>

const char *const sdl_tbmf::errors::Error::mErrorStrings[etNumberOfErrors] = {
	"No error",
	"Unknown error",
	"Out of memory",
	"File not found or permission denied",
	"File format was invalid",
	"Could not read file",
	"Could not create surface",
	"Could not lock surface",
	"Tried to access data outside of array",
	"Index 0 is invalid for character map",
	"Character map does not contain requsted character",
	"Could not set palette for surface"
};

void sdl_tbmf::errors::Error::print() const {
	std::cerr << mErrorStrings[mType] << ":\n" << mString;
}
