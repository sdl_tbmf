#ifndef SDL_TBMF_UNCOMP_H
#define SDL_TBMF_UNCOMP_H

#include <sys/types.h>
#include "SDL_tbmf_data.h"

namespace sdl_tbmf {

bool uncompressData(Data *data, u_char specialByte = 0xcc);

} // namespace sdl_tbmf

#endif /* SDL_TBMF_UNCOMP_H */
