#include "SDL_tbmf_errors.h"
#include "SDL_tbmf_string.h"
#include <cstring>

namespace sdl_tbmf {

using namespace sdl_tbmf::errors;

char *copyString(const char *);

TString::TString() {
	mData = copyString("");
}

TString::TString(const TString &other) {
	mData = copyString(other.mData);
}

TString::TString(const char *s) {
	mData = copyString(s);
}

TString::~TString() {
	delete mData;
}

TString &TString::operator=(const TString &other) {
	if (this == &other) return *this;
	delete mData;
	mData = copyString(other.mData);
	return *this;
}

unsigned int TString::length() const {
	return strlen(mData);
}

char *copyString(const char *s) {
	char *d;

	if (s == NULL) {
		d = new char[1];
		if (!d) throw Error(etNoMem);
		d[0] = 0;
		return d;
	}

	size_t l = strlen(s) + 1;
	d = new char[l];
	if (!d) throw Error(etNoMem);
	return strncpy(d, s, l);
}

} // namespace sdl_tbmf
