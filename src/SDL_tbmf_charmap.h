#ifndef SDL_TBMF_CHARMAP_H
#define SDL_TBMF_CHARMAP_H

#include "SDL.h"

namespace sdl_tbmf {

class CharacterMap {
public:
	CharacterMap();
	CharacterMap(const CharacterMap &);
	~CharacterMap(); // NOTICE: ~CharacterMap() frees SDL_Surfaces.

	CharacterMap &operator=(const CharacterMap &);

	SDL_Surface *setCharacter(Uint32, int, int);
	bool hasCharacter(Uint32) const;
	SDL_Surface *getCharacter(Uint32) const;

	SDL_Surface *operator[](Uint32 i) const { return getCharacter(i); }

private:
	struct Node {
		SDL_Surface *data;
		Node *child0;
		Node *child1;
	};

	Node *mRoot;
	mutable Node *mShortcut;
	mutable Uint32 mShortcutPos;

	Node *find(Uint32) const;
	void remove(Node *);
	Node *copy(Node *);
};

} // namespace sdl_tbmf

#endif /* SDL_TBMF_CHARMAP_H */
