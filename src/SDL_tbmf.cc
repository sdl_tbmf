#include "SDL_tbmf.h"
#include "SDL_tbmf_charmap.h"
#include "SDL_tbmf_errors.h"
#include "SDL_tbmf_string.h"
#include "SDL_tbmf_uncomp.h"
#include <cstring>
#include <fstream>

namespace sdl_tbmf {

using namespace sdl_tbmf::errors;
using std::ifstream;
using std::ios;

void readHeader(ifstream &, TString &, Uint8 &, bool &, Uint8 &, Uint8 &,
		wchar_t &, u_char &);
int readCharacter(ifstream &, CharacterMap &, Uint8, Uint8, bool, u_char);
void closeFile(ifstream &);

Font::Font():
 mDescription(),
 mChrGraphs() {
	mChrGraphs.setCharacter(1, 0, 0);
	mHeight = 0;
	mBaseline = 0;
	mBitsPerPixel = 0;
	mDefaultChar = 1;
}

Font::Font(const char *f) {
	loadFile(f);
}

Font::Font(const Font &other):
 mChrGraphs(other.mChrGraphs),
 mDescription(other.mDescription) {
	mHeight = other.mHeight;
	mBaseline = other.mBaseline;
	mBitsPerPixel = other.mBitsPerPixel;
	mDefaultChar = other.mDefaultChar;
}

Font::~Font() {}

Font &Font::operator=(const Font &other) {
	mChrGraphs = other.mChrGraphs;
	mDescription = other.mDescription;
	mHeight = other.mHeight;
	mBaseline = other.mBaseline;
	mBitsPerPixel = other.mBitsPerPixel;
	mDefaultChar = other.mDefaultChar;

	return *this;
}

void Font::loadFile(const char *fileName) {
	ifstream fontFile(fileName, ios::binary);
	if (!fontFile) throw Error(etFileOpen, fileName);

	bool comp;
	u_char special;

	readHeader(fontFile, mDescription, mBitsPerPixel, comp, mHeight,
		   mBaseline, mDefaultChar, special);
	while (readCharacter(fontFile, mChrGraphs, mBitsPerPixel, mHeight,
			     comp, special));
	closeFile(fontFile);

	if (!mChrGraphs.hasCharacter(mDefaultChar))
		throw Error(etFileFormat, "No default character in file.");
}

void readHeader(ifstream &file, TString &desc, Uint8 &bpp, bool &cmp,
		Uint8 &h, Uint8 &bl, wchar_t &dc, u_char &sb) {
	char buf[256];

	// IDENTIFIER
	file.read((char *)&buf, 16);
	if (file.fail()) {
		file.close();
		throw Error(etFileFormat, "No (correct) identifier.");
	}
	if (strncmp(buf, "TuMesBitMapFont", 16)) {
		file.close();
		throw Error(etFileFormat,
			    "Identifier part was invalid. (Not a TBMF file)");
	}

	// DESCRIPTION
	buf[255] = 0;
	for (int i = 0; i < 256; ++i) {
		buf[i] = file.get();
		if (buf[i] == 0) break;
	}
	if (buf[255] != 0) {
		file.close();
		throw Error(etFileFormat,
			    "Description part was invalid. (too long)");
	}

	desc = buf;

	// INFO-BYTE
	u_char byt0, byt1;

	byt0 = file.get();

	bpp = 1 << (byt0 & 3);
	cmp = byt0 & 128; // Uses compression?

	// HEIGHT
	h = file.get();
	if (h == 0) {
		file.close();
		throw Error(etFileFormat, "Invalid (=zero) font height.");
	}

	// BASELINE
	bl = file.get();

	// DEFAULT CHARACTER
	byt0 = file.get();
	byt1 = file.get();
	dc = wchar_t((byt1 << 8) | byt0);
	if (dc == 0) {
		file.close();
		throw Error(etFileFormat,
			    "Default character number is 0, "
			    "which is invalid.");
	}

	// SPECIAL BYTE
	sb = 0;
	if (cmp) sb = file.get();

	if (file.eof()) {
		file.close();
		throw Error(etFileFormat, "End of file reached too early.");
	}
}

int readCharacter(ifstream &file, CharacterMap &chrMap, Uint8 bpp, Uint8 h,
		  bool comp, u_char special) {
	if (bpp != 4) { // only 4 bits/pixel is currently supported
		file.close();
		throw Error(etFileRead,
			    "This function can currently "
			    "read only 4 bpp fonts.");
	}

	u_char byt0, byt1;

	byt0 = file.get();
	if (byt0 == 0 && file.peek() == 0) { // End of font reached
		file.putback(byt0);
		return 0;
	}

	byt1 = file.get();

	wchar_t tc = wchar_t((byt1 << 8) | byt0); // Number of character

	if (file.eof()) {
		file.close();
		throw Error(etFileFormat, "End of file reached too early.");
	}

	u_char cWidth;
	Data data;
	if (comp) {
		byt0 = file.get();
		byt1 = file.get();
		data.mSize = (byt1 << 8) | byt0;
		data.mData = new u_char[data.mSize];
		if (data.mData == NULL) throw Error(etNoMem);
		byt0 = file.get(); // Compression type
		for (Uint32 i = 0; i < data.mSize; ++i)
			data.mData[i] = file.get();
		switch (byt0) {
			case 0:
			break;

			case 1:
			if (!uncompressData(&data, special))
				throw Error(etFileRead,
					    "Problems with uncompression.");
			break;

			default:
			throw Error(etFileRead, "Unsupported compression");
		}
		cWidth = data.mData[0];
	}
	else {
		cWidth = file.get();
		data.mSize = (cWidth * h + 1) / 2 + 1;
		data.mData = new u_char[data.mSize];
		if (data.mData == NULL) throw Error(etNoMem);
		data.mData[0] = cWidth;
		for (Uint32 i = 1; i < data.mSize; ++i)
			data.mData[i] = file.get();
	}

	SDL_Surface *surf = chrMap.setCharacter(tc, cWidth, h);

	if (SDL_LockSurface(surf) != 0)
		throw Error(etSurfaceLock, SDL_GetError());

	Uint32 *ptr = (Uint32 *)surf->pixels;
	Uint32 *maxptr = ptr + surf->w * surf->h;

	for (Uint32 i = 0, p = 1; i < Uint32(cWidth * h); ++i) {
		if (i % 2 == 0) {
			if (p < data.mSize) byt0 = data.mData[p++];
			else throw Error(etOutsideArray); // Should not happen
			if (ptr < maxptr) {
				*ptr = SDL_MapRGBA(surf->format, 0, 0, 0,
						   ~((byt0 & 15) * 17));
				++ptr;
			}
			if (ptr < maxptr) {
				*ptr = SDL_MapRGBA(surf->format, 0, 0, 0,
						   ~(((byt0 & 240) >> 4) * 17)
					);
				++ptr;
			}
		}
	}
	SDL_UnlockSurface(surf);
	delete data.mData;

	return 1;
}

void closeFile(ifstream &file) {
	char buffer[13];
	const char *s = "EndOfFont";

	file.read((char *)&buffer, 13);
	if (!file.eof()) {
		file.close();
		throw Error(etFileFormat,
			    "End of file determinator reached too early.");
	}

	file.close();
	for (int i = 0; i < 10; ++i) {
		if (s[i] == buffer[i + 2]) continue;
		throw Error(etFileFormat, "End of file was invalid.");
	}
}

} // namespace sdl_tbmf
