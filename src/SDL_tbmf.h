#ifndef SDL_TBMF_H
#define SDL_TBMF_H

#include "SDL.h"
#include "SDL_tbmf_charmap.h"
#include "SDL_tbmf_string.h"

namespace sdl_tbmf {

class Font {
public:
	Font();
	Font(const char *);
	Font(const Font &);
	~Font();

	Font &operator=(const Font &);

	bool hasCharacter(wchar_t c) const {
		return mChrGraphs.hasCharacter(c);
	}
	inline const SDL_Surface *getCharacter(wchar_t) const;
	inline int getCharacterWidth(wchar_t) const;
	int height() const { return mHeight; }
	int baseline() const { return mBaseline; }
	const char *description() const { return mDescription; }
	int bitsPerPixel() const { return mBitsPerPixel; }
	wchar_t defaultCharacter() const { return mDefaultChar; }


private:
	CharacterMap mChrGraphs;
	TString mDescription;
	Uint8 mHeight, mBaseline, mBitsPerPixel;
	wchar_t mDefaultChar;

	void loadFile(const char *);
};

const SDL_Surface *Font::getCharacter(wchar_t c) const {
	if (mChrGraphs.hasCharacter(c)) return mChrGraphs.getCharacter(c);
	else return mChrGraphs.getCharacter(mDefaultChar);
}

int Font::getCharacterWidth(wchar_t c) const {
	if (mChrGraphs.hasCharacter(c)) return mChrGraphs.getCharacter(c)->w;
	else return mChrGraphs.getCharacter(mDefaultChar)->w;
}

} // namespace sdl_tbmf

#endif /* SDL_TBMF_H */
