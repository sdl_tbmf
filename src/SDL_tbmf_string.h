#ifndef SDL_TBMF_STRING_H
#define SDL_TBMF_STRING_H

namespace sdl_tbmf {

/** Very simple string class for Font and TextSurface internal use.
 */
class TString {
public:
	TString();
	TString(const TString &);
	TString(const char *);
	~TString();

	TString &operator=(const TString &);
	operator char *() { return mData; }
	operator const char *() const { return mData; }
	char &operator[](unsigned int i) { return mData[i]; }
	const char &operator[](unsigned int i) const { return mData[i]; }

	unsigned int length() const;

private:
	char *mData;
};


} // namespace sdl_tbmf

#endif /* SDL_TBMF_STRING_H */
