#include "SDL.h"
#include "SDL_tbmf_charmap.h"
#include "SDL_tbmf_errors.h"
#include <cstring>

namespace sdl_tbmf {

using namespace sdl_tbmf::errors;

SDL_Surface *copySurface(const SDL_Surface *);

CharacterMap::CharacterMap() {
	mRoot = new Node;
	if (!mRoot) throw Error(etNoMem);
	mRoot->data = NULL;
	mRoot->child0 = NULL;
	mRoot->child1 = NULL;
	mShortcut = NULL;
	mShortcutPos = 0;
}

CharacterMap::CharacterMap(const CharacterMap &o) {
	mRoot = copy(o.mRoot);
	mShortcutPos = 0;
	mShortcut = find(o.mShortcutPos);
	mShortcutPos = o.mShortcutPos;
}

CharacterMap::~CharacterMap() {
	remove(mRoot);
}

CharacterMap &CharacterMap::operator=(const CharacterMap &o) {
	if (this != &o) {
		remove(mRoot);
		mRoot = copy(o.mRoot);
		mShortcutPos = 0;
		mShortcut = find(o.mShortcutPos);
		mShortcutPos = o.mShortcutPos;
	}
	return *this;
}

SDL_Surface *CharacterMap::setCharacter(Uint32 i, int w, int h) {
	if (i == 0) throw Error(etCharMapZeroIndex);
	Uint32 m = 0x80000000; // 2^31
	while (!(i & m)) m >>= 1;

	Node *ptrNode, **child;

	ptrNode = mRoot;

	while (m >>= 1) {
		if ((i & m) == 0) 
			child = &ptrNode->child0;
		else
			child = &ptrNode->child1;

		if (!*child) {
			*child = new Node;
			if (!*child) throw Error(etNoMem);
			(*child)->data = NULL;
			(*child)->child0 = NULL;
			(*child)->child1 = NULL;
		}

		ptrNode = *child;
	}

	SDL_Surface *surf;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	surf = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 32,
				    0xff000000, 0xff0000, 0xff00, 0xff);
#else
	surf = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 32,
				    0xff, 0xff00, 0xff0000, 0xff000000);
#endif

	if (surf == NULL) throw Error(etSurfaceCreate, SDL_GetError());

	if (ptrNode->data) SDL_FreeSurface(ptrNode->data);

	ptrNode->data = surf;

	mShortcut = ptrNode;
	mShortcutPos = i;
	return surf;
}

bool CharacterMap::hasCharacter(Uint32 i) const {
	Node *node = find(i);
	if (!node) return false;
	mShortcut = node;
	mShortcutPos = i;
	return true;
}

SDL_Surface *CharacterMap::getCharacter(Uint32 i) const {
	Node *node = find(i);
	if (node) return node->data;
	throw Error(etCharMapNoNode);
}

CharacterMap::Node *CharacterMap::find(Uint32 i) const {
	if (i == 0) return NULL;

	if (mShortcutPos == i) return mShortcut;

	Uint32 m = 0x80000000; // 2^31
	while (!(i & m)) m >>= 1;

	Node *ptrNode = mRoot;

	while (m >>= 1) {
		if ((i & m) == 0)
			ptrNode = ptrNode->child0;
		else
			ptrNode = ptrNode->child1;

		if (!ptrNode) return NULL;
	}

	return ptrNode;
}

void CharacterMap::remove(CharacterMap::Node *node) {
	if (node == NULL) return;

	remove(node->child0);
	remove(node->child1);

	if (node->data)	SDL_FreeSurface(node->data);
	delete node;
}

CharacterMap::Node *CharacterMap::copy(CharacterMap::Node *node) {
	if (node == NULL) return NULL;

	Node *newNode = new Node;
	if (!newNode) throw Error(etNoMem);
	newNode->data = copySurface(node->data);

	newNode->child0 = copy(node->child0);
	newNode->child1 = copy(node->child1);

	return newNode;
}

SDL_Surface *copySurface(const SDL_Surface *src) {
	if (src == NULL) return NULL; // Yes, it is a copy ;-)

	SDL_Surface *dst = SDL_CreateRGBSurface(src->flags, src->w, src->h,
						src->format->BitsPerPixel,
						src->format->Rmask,
						src->format->Gmask,
						src->format->Bmask,
						src->format->Amask);
	if (dst == NULL) throw Error(etSurfaceCreate, SDL_GetError());

	Uint8 *srcp = (Uint8 *)src->pixels;
	Uint8 *dstp = (Uint8 *)dst->pixels;
	int h = src->h;
	int w = src->w * src->format->BytesPerPixel;
	while (h--) {
		memcpy(dstp, srcp, w);
		srcp += src->pitch;
		dstp += dst->pitch;
	}

	if (src->format->palette != NULL) {
		SDL_Palette *srcpal = src->format->palette;

		SDL_Color colors[256];
		int n = srcpal->ncolors;
		if (n > 256) n = 256;
		for (int i = 0; i < n; ++i) colors[i] = srcpal->colors[i];

		if(!SDL_SetPalette(dst, SDL_LOGPAL|SDL_PHYSPAL, colors, 0, n))
			throw Error(etPaletteSet, SDL_GetError());
	}
	return dst;
}

} // namespace sdl_tbmf
