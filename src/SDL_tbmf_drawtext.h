#ifndef SDL_TBMF_DRAWTEXT_H
#define SDL_TBMF_DRAWTEXT_H

#include "SDL.h"
#include "SDL_tbmf.h"

namespace sdl_tbmf {

/** Draw given text to surface (to some coordinates) with the given font.
 */
void drawText(SDL_Surface *, Sint16, Sint16, const char *, Font &);

/** Draw given text to area in surface.
 * Does not draw outside the given area.
 * @return 1 if text fitted to area, 0 otherwise
 */
int drawTextBox(SDL_Surface *, Sint16, Sint16, Sint16, Sint16,
		const char *, Font &);

inline int drawText(SDL_Surface *s, Sint16 x, Sint16 y, Sint16 w, Sint16 h,
		    const char *t, Font &f) {
	return drawTextBox(s, x, y, w, h, t, f);
}

} // namespace sdl_tbmf

#endif /* SDL_TBMF_DRAWTEXT_H */
